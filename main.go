package main

import "C"
import (
	"bufio"
	"flag"
	"fmt"
	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/giorgisio/goav/avcodec"
	"github.com/giorgisio/goav/avformat"
	"github.com/giorgisio/goav/avutil"
	"github.com/giorgisio/goav/swresample"
	"github.com/giorgisio/goav/swscale"
	"log"
	"os"
	"time"
	"unsafe"
)

func main() {
	argFilename := flag.String("i", "", "The input file.")
	argWidth := flag.Int("w", 64, "The output width.")
	argHeight := flag.Int("h", 36, "The output height.")
	argRate := flag.Int("r", 15, "The output framerate.")
	argAudio := flag.Bool("a", false, "Whether to play audio.")

	flag.Parse()

	if len(*argFilename) == 0 {
		log.Fatal("Filename mustn't be empty.")
	}

	filename := *argFilename
	size := Size{
		Width:  *argWidth,
		Height: *argHeight,
	}
	framerate := *argRate
	packetBuffer := 32768

	avformat.AvRegisterAll()
	ctx := avformat.AvformatAllocContext()
	defer ctx.AvformatFreeContext()

	if avformat.AvformatOpenInput(&ctx, filename, nil, nil) != 0 {
		log.Fatal("Could not open input file.")
	}

	if ctx.AvformatFindStreamInfo(nil) < 0 {
		log.Fatal("No stream information found in file.")
	}

	vCtx, aCtx, err := openCodecs(ctx, *argAudio)
	if err != nil {
		log.Fatal(err)
	}
	// todo: somehow calling avcodec_free_context causes a segfault.
	// defer vCtx.AvcodecFreeContext()
	if *argAudio {
		// defer aCtx.AvcodecFreeContext()
	}
	if framerate > vCtx.rate {
		framerate = vCtx.rate
	}

	done := make(chan bool)

	vDecodeSync := make(chan bool)
	go handleVideo(vCtx, size, vDecodeSync, framerate, done)

	videoPacketChan := make(chan *avcodec.Packet, 1024)
	go codecSender(vCtx, videoPacketChan, vDecodeSync)

	var audioPacketChan chan *avcodec.Packet
	if *argAudio {
		aDecodeSync := make(chan bool)
		go handleAudio(aCtx, aDecodeSync, done)

		audioPacketChan = make(chan *avcodec.Packet, 512)
		go codecSender(aCtx, audioPacketChan, aDecodeSync)
	}

	resp := 0
	for resp >= 0 {
		packet := avcodec.AvPacketAlloc()
		// give this packet its own buffer, so that it doesnt get invalidated on the next AvReadFrame call
		// this is so that we can buffer it before sending it to the decoder
		packet.AvNewPacket(packetBuffer)

		resp = ctx.AvReadFrame(packet)
		if resp == avutil.AvErrorEOF {
			videoPacketChan <- nil
			if *argAudio {
				audioPacketChan <- nil
			}
			packet.AvPacketUnref()
			break
		} else if resp < 0 {
			log.Fatalf("Error while reading packet: %d %s\n", resp, avutil.ErrorFromCode(resp))
		}

		// todo: after running for some minutes, the audio packet buffer seems to run dry while waiting for the video buffer.
		//  solution (maybe): put another buffer after decoding, resampling and skipping, this would need less memory

		if packet.StreamIndex() == vCtx.streamIndex {
			videoPacketChan <- packet
		} else if *argAudio && packet.StreamIndex() == aCtx.streamIndex {
			audioPacketChan <- packet
		} else {
			// the decoder thread will free the packet, but when it doesn't belong to any stream we're interested in
			// decoding, we need to free it here
			packet.AvPacketUnref()
		}
	}
	// shut down decoder threads
	close(videoPacketChan)
	if *argAudio {
		close(audioPacketChan)
	}

	// wait for consumer threads to finish
	<-done
	if *argAudio {
		<-done
	}
}

func codecSender(codec *avContextEx, packets chan *avcodec.Packet, decodeSync chan bool) {
	for packet := range packets {
		resp := codec.AvcodecSendPacket(packet)

		// avcodec_send_packet interprets a nil packet as a flush packet at EOF, but unref doesn't handle this
		if packet != nil {
			packet.AvPacketUnref()
		}
		decodeSync <- true
		if resp < 0 {
			log.Fatalf("error while sending packet to %d: %d %s\n", codec.streamIndex, int(resp), avutil.ErrorFromCode(resp))
		}
		<-decodeSync
	}
}

type avContextEx struct {
	*avcodec.Context
	streamIndex int
	rate        int
}

type Size struct {
	Width, Height int
}

func openCodecs(ctx *avformat.Context, includeAudio bool) (videoCtx, audioCtx *avContextEx, err error) {
	for i := 0; i < int(ctx.NbStreams()); i++ {
		codecType := ctx.Streams()[i].CodecParameters().AvCodecGetType()

		if codecType != avformat.AVMEDIA_TYPE_VIDEO &&
			(codecType != avformat.AVMEDIA_TYPE_AUDIO || !includeAudio) {
			continue
		}
		codecCtxOrig := ctx.Streams()[i].Codec()

		codec := avcodec.AvcodecFindDecoder(avcodec.CodecId(codecCtxOrig.GetCodecId()))
		if codec == nil {
			err = fmt.Errorf("unsupported codec for stream %d", i)
			break
		}

		newCtx := &avContextEx{
			Context:     codec.AvcodecAllocContext3(),
			streamIndex: i,
		}
		if newCtx.AvcodecCopyContext((*avcodec.Context)(unsafe.Pointer(codecCtxOrig))) != 0 {
			err = fmt.Errorf("error while copying context")
			break
		}

		if newCtx.AvcodecOpen2(codec, nil) < 0 {
			err = fmt.Errorf("could not open codec")
			break
		}

		if codecType == avformat.AVMEDIA_TYPE_VIDEO {
			videoCtx = newCtx
			rate := ctx.Streams()[i].AvgFrameRate()
			videoCtx.rate = rate.Num() / rate.Den()
		} else {
			audioCtx = newCtx
			audioCtx.rate = audioCtx.SampleRate()
		}

		(*avcodec.Context)(unsafe.Pointer(codecCtxOrig)).AvcodecClose()
	}

	if err != nil {
		if videoCtx != nil && videoCtx.AvcodecIsOpen() > 0 {
			videoCtx.AvcodecClose()
		}
		if audioCtx != nil && audioCtx.AvcodecIsOpen() > 0 {
			audioCtx.AvcodecClose()
		}
		return nil, nil, err
	}
	return
}

func handleVideo(vCtx *avContextEx, outputSize Size, sync chan bool, framerate int, done chan bool) {
	scaler := swscale.SwsGetcontext(
		vCtx.Context.Width(),
		vCtx.Context.Height(),
		swscale.PixelFormat(vCtx.Context.PixFmt()),
		outputSize.Width,
		outputSize.Height,
		avcodec.AV_PIX_FMT_RGB24,
		avcodec.SWS_BILINEAR,
		nil,
		nil,
		nil,
	)

	srcFrame := avutil.AvFrameAlloc()
	defer avutil.AvFrameFree(srcFrame)
	destFrame := prepareDestinationFrame(outputSize.Width, outputSize.Height)
	defer avutil.AvFrameFree(destFrame)
	// todo: close the buffer that destFrame is based on, but it's buried in the sub-function
	//  edit: the buffer might be closed as part of av_frame_free, not sure though

	bufOut := bufio.NewWriterSize(os.Stdout, outputSize.Width*outputSize.Height*3)

	var resp int
	skipRate := vCtx.rate / framerate
	counter := skipRate
	frameDuration := time.Duration(float32(1) / float32(framerate) * float32(time.Second))
	frameFinish := time.Now()

	for true {
		<-sync
		resp = 0
		for resp >= 0 {
			resp = vCtx.AvcodecReceiveFrame((*avcodec.Frame)(unsafe.Pointer(srcFrame)))
			if resp == avutil.AvErrorEAGAIN {
				break
			} else if resp == avutil.AvErrorEOF {
				sync <- true
				done <- true
				return
			} else if resp < 0 {
				log.Fatalf("error while receiving video frame from decoder: %s\n",
					avutil.ErrorFromCode(resp))
			}

			if counter == skipRate {
				counter = 0

				swscale.SwsScale2(scaler,
					avutil.Data(srcFrame), avutil.Linesize(srcFrame),
					0, vCtx.Context.Height(),
					avutil.Data(destFrame), avutil.Linesize(destFrame))

				_, _ = fmt.Fprint(bufOut, "\033[H")
				imageData := avutil.Data(destFrame)[0]
				for y := 0; y < outputSize.Height; y++ {
					startPos := uintptr(unsafe.Pointer(imageData)) + uintptr(y)*uintptr(outputSize.Width*3)
					for x := 0; x < outputSize.Width; x++ {
						rgb := *(*[3]uint8)(unsafe.Pointer(startPos + uintptr(x*3)))
						pixel := fmt.Sprintf("\x1b[38;2;%d;%d;%dm██", rgb[0], rgb[1], rgb[2])
						_, _ = fmt.Fprint(bufOut, pixel)
					}
					_, _ = fmt.Fprint(bufOut, "\n")
				}
				_ = bufOut.Flush()

				// this approach to timing uses the fixed reference of the first frameFinish
				// and propagates it with the arithmetic precision of adding frameTime to it.
				// An old approach was based on trying to time each frame to be exactly frameTime
				// long, with no fixed base, and thus accumulating all errors.
				frameFinish = frameFinish.Add(frameDuration)
				time.Sleep(time.Until(frameFinish))
			}
			counter++
		}

		sync <- true
	}
}

func prepareDestinationFrame(width, height int) *avutil.Frame {
	frame := avutil.AvFrameAlloc()
	if frame == nil {
		log.Fatal("Unable to allocate destination frame")
	}

	numBytes := uintptr(avcodec.AvpictureGetSize(avcodec.AV_PIX_FMT_RGB24,
		width, height))
	buffer := avutil.AvMalloc(numBytes)

	avp := (*avcodec.Picture)(unsafe.Pointer(frame))
	avp.AvpictureFill((*uint8)(buffer), avcodec.AV_PIX_FMT_RGB24, width, height)

	return frame
}

func handleAudio(aCtx *avContextEx, sync chan bool, done chan bool) {
	sampleRate := beep.SampleRate(44100)

	resampler := swresample.SwrAlloc().SwrAllocSetOpts(
		avutil.AV_CH_LAYOUT_STEREO, swresample.AV_SAMPLE_FMT_DBLP, sampleRate.N(time.Second),
		avutil.AvGetDefaultChannelLayout(aCtx.Channels()),
		swresample.AvSampleFormat(aCtx.SampleFmt()), aCtx.SampleRate(), 0, 0)

	decodeFrame := avutil.AvFrameAlloc()
	defer avutil.AvFrameFree(decodeFrame)
	resampledFrame := avutil.AvFrameAlloc()
	defer avutil.AvFrameFree(resampledFrame)

	avutil.AvSetFrameAudio(resampledFrame, sampleRate.N(time.Second),
		avutil.AV_CH_LAYOUT_STEREO, swresample.AV_SAMPLE_FMT_DBLP)

	stream := &ffmpegStreamer{
		stream: make(chan *[2]float64, sampleRate.N(time.Millisecond*50)),
	}

	_ = speaker.Init(sampleRate, sampleRate.N(time.Millisecond*30))
	speaker.Play(stream)

	var resp int
	for true {
		<-sync
		resp = 0
		for resp >= 0 {
			resp = aCtx.AvcodecReceiveFrame((*avcodec.Frame)(unsafe.Pointer(decodeFrame)))
			if resp == avutil.AvErrorEAGAIN {
				break
			} else if resp == avutil.AvErrorEOF {
				sync <- true
				done <- true
				return
			} else if resp < 0 {
				log.Fatalf("error while receiving audio frame from decoder: %s\n",
					avutil.ErrorFromCode(resp))
			}

			resampler.SwrConvertFrame((*swresample.Frame)(unsafe.Pointer(resampledFrame)), (*swresample.Frame)(unsafe.Pointer(decodeFrame)))

			planes := avutil.Data(resampledFrame)

			// i thought linesize/8 would be accurate, but it wasn't, now using the official sample number
			for i := 0; i < avutil.AvFrameNbSamples(resampledFrame); i++ {
				left := (*float64)(unsafe.Pointer(uintptr(unsafe.Pointer(planes[0])) + uintptr(i*8)))
				right := (*float64)(unsafe.Pointer(uintptr(unsafe.Pointer(planes[1])) + uintptr(i*8)))

				stream.stream <- &[2]float64{*left, *right}
			}
		}
		sync <- true
	}
}

type ffmpegStreamer struct {
	stream  chan *[2]float64
	lastErr error
}

func (f *ffmpegStreamer) Stream(samples [][2]float64) (n int, ok bool) {
loop:
	for n < len(samples) {
		select {
		case smp := <-f.stream:
			samples[n] = *smp
		default:
			break loop
		}
		n++
	}

	return n, true
}

func (f *ffmpegStreamer) Err() error {
	return f.lastErr
}
